#include <iostream>
#include <string>
#include "Graph.h"
using namespace std;

void main()
{
	int data[4][4] = { {},{},{},{} };
	cout << "Create your graph" << endl;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			cout << "Row:" << i + 1 << "Column:" << j + 1 << endl << "input: ";
			cin >> data[i][j];
			cout << endl;
		}
	}

	Graph g;
	g.insert(data);
	g.printGraph();
	if (g.multigraph())
	{
		cout << "This graph is multigraph" << endl;
	}
	if (g.pseudograph())
	{
		cout << "This graph is pseudograph" << endl;
	}
	if (g.digraph())
	{
		cout << "This graph is digraph" << endl;
	}
	if (g.CompleteGraph())
	{
		cout << "This graph is completegraph" << endl;
	}
	if (g.WeightedGraph())
	{
		cout << "This graph is weightedGraph" << endl;
	}
	g.dijkstra(0);
	g.primsmst();

	system("pause");
	
}